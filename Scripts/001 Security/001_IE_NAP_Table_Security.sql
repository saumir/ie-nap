/*====================================================================================================

CREATE TABLE (SECURITY)

=====================================================================================================*/
-- SERVER:        ---
-- Database:      ---
-- TABLE:         Users
-- =======================================
-- CREATED by Saumir Navarro, on 23/11/2015
-- MODIFIED by ---

 

/*====================================================================================================
1. Select Database
=====================================================================================================*/

USE  ie_nap;

/*====================================================================================================
2. BEGIN TRANSACTION
=====================================================================================================*/

START TRANSACTION;

/*====================================================================================================
3. CREATE TABLE Users
=====================================================================================================*/

DROP TABLE IF EXISTS Users;


CREATE TABLE Users(
	UserID int AUTO_INCREMENT NOT NULL,
	UserName varchar (50) NOT NULL,
    UserEmail varchar (50) NOT NULL,
	UserLogin varchar (10) NOT NULL,
	UserPassword varchar (10) NOT NULL,
	UserDateCreated datetime NOT NULL,
	UserCreatorID int NOT NULL,
    UserIsActive bit  NOT NULL DEFAULT 1, /*True=Yes; False=No*/
    UserIsDelete bit  NOT NULL DEFAULT 0, /*True=Yes; False=No*/
 PRIMARY KEY (UserID));
 





/*====================================================================================================
4. END TRANSACTION
=====================================================================================================*/
-- ROLLBACK /* if the data sample after the change does not match the results. */
-- COMMIT   /* when the sample data after the change coincides with the results. */