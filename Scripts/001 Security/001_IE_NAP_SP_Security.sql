/*====================================================================================================

CREATE STORED PROCEDURES (SECURITY)

=====================================================================================================*/
-- SERVER:        ---
-- Database:      ---
-- TABLE:         Users
-- =======================================
-- CREATED by Saumir Navarro, on 23/11/2015
-- MODIFIED by ---
 

/*====================================================================================================
1. Select Database
=====================================================================================================*/

USE  ie_nap;

/*====================================================================================================
2. BEGIN TRANSACTION
=====================================================================================================*/

START TRANSACTION;


/*====================================================================================================
3. CREATE SP UsersLogin
=====================================================================================================*/

DROP PROCEDURE IF EXISTS UsersLogin;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 23/11/2015
	Description:	<Get User Login>
=============================================*/

CREATE PROCEDURE UsersLogin(
_userLogin VARCHAR(15),
_userPassword VARCHAR(15)
)
BEGIN
		
		SELECT UserID, UserName, UserLogin, UserPassword			
		FROM ie_nap.Users
		WHERE UserIsActive=1 
			AND UserLogin = _userLogin
			AND UserPassword=_userPassword;
			 
		
END||UsersLogin

DELIMITER ;

/*====================================================================================================
4. CREATE SP UsersEmail
=====================================================================================================*/

DROP PROCEDURE IF EXISTS UsersEmail;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 23/11/2015
	Description:	<Get User Email>
=============================================*/

CREATE PROCEDURE UsersEmail(
_userEmail VARCHAR(50)
)
BEGIN
		
		SELECT UserID, UserName, UserLogin, UserPassword		
		FROM ie_nap.Users u
		WHERE UserIsActive=1 
			AND UserEmail = _userEmail;
			
			 
		
END||UsersEmail

DELIMITER ;





/*====================================================================================================
5. FIN DE LA TRANSACCION
=====================================================================================================*/
-- ROLLBACK /*en caso de que la muestra de datos despues del cambio no coincida con los resultados. */
-- COMMIT   /*cuando la muestra de datos despues del cambio coincide con los resultados. */