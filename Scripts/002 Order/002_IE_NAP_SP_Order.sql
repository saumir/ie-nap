/*====================================================================================================

CREATE STORED PROCEDURES (ORDER)

=====================================================================================================*/
-- SERVER:        ---
-- Database:      ---
-- TABLE:         Orders
-- =======================================
-- CREATED by Saumir Navarro, on 23/11/2015
-- MODIFIED by ---
 

/*====================================================================================================
1. Select Database
=====================================================================================================*/

USE  ie_nap;

/*====================================================================================================
2. BEGIN TRANSACTION
=====================================================================================================*/

START TRANSACTION;


/*====================================================================================================
3. CREATE SP AddOrders
=====================================================================================================*/

DROP PROCEDURE IF EXISTS AddOrders;


/*=============================================
	CREATED by Saumir Navarro, on 27/05/2011
	Description:	<Add Orders>
=============================================*/

DELIMITER ||

CREATE PROCEDURE AddOrders(
_StateID int,
_OrderDate datetime,
_OrderFirstName varchar(100),
_OrderLastName varchar(100),
_OrderAddress varchar(255),
_OrderCity varchar(100),
_OrderZip varchar(100),
_OrderPhone varchar(50),
_OrderAlt varchar(50),
_OrderEmail varchar(50),
_OrderVIN varchar(50),
_OrderMileage varchar(100),
_OrderYearA int,
_OrderMake varchar(50),
_OrderModel varchar(50),
_OrderYearB int,
_OrderDP varchar(50),
_OrderMonthly int,
_OrderTerms varchar(100),
_OrderPolicyType varchar(255),
_OrderSaleDate datetime,
_OrderPostDate datetime,
_OrderFronter varchar(50),
_OrderCloser varchar(50),
_OrderPaymentInfo varchar(4),
_OrderNameOnCard varchar(100),
_OrderCC varchar(50),
_OrderExpDate datetime,
_OrderCVV varchar(50),
_OrderRouting varchar(50),
_OrderAcct varchar(50),
_OrderCheck varchar(50),
_OrderTemporaryPolicy varchar(255),
_OrderCreatorID int
)
BEGIN
	#SET NOCOUNT ON;
	
		INSERT INTO ie_nap.Orders (
			StateID,
			OrderDate,
			OrderFirstName,
			OrderLastName,
			OrderAddress,
			OrderCity,
			OrderZip,
			OrderPhone,
			OrderAlt,
			OrderEmail,
			OrderVIN,
			OrderMileage,
			OrderYearA,
			OrderMake,
			OrderModel,
			OrderYearB,
			OrderDP,
			OrderMonthly,
			OrderTerms,
			OrderPolicyType,
			OrderSaleDate,
			OrderPostDate,
			OrderFronter,
			OrderCloser,
			OrderPaymentInfo,
			OrderNameOnCard,
			OrderCC,
			OrderExpDate,
			OrderCVV,
			OrderRouting,
			OrderAcct,
			OrderCheck,
			OrderTemporaryPolicy,
			OrderDateCreated,
			OrderCreatorID)
		VALUES (
			_StateID,
			_OrderDate,
			_OrderFirstName,
			_OrderLastName,
			_OrderAddress,
			_OrderCity,
			_OrderZip,
			_OrderPhone,
			_OrderAlt,
			_OrderEmail,
			_OrderVIN,
			_OrderMileage,
			_OrderYearA,
			_OrderMake,
			_OrderModel,
			_OrderYearB,
			_OrderDP,
			_OrderMonthly,
			_OrderTerms,
			_OrderPolicyType,
			_OrderSaleDate,
			_OrderPostDate,
			_OrderFronter,
			_OrderCloser,
			_OrderPaymentInfo,
			_OrderNameOnCard,
			_OrderCC,
			_OrderExpDate,
			_OrderCVV,
			_OrderRouting,
			_OrderAcct,
			_OrderCheck,
			_OrderTemporaryPolicy,
			NOW(),
			_OrderCreatorID
			);
		
		
END||

DELIMITER ;




/*====================================================================================================
4. CREATE SP EditOrders
=====================================================================================================*/

DROP PROCEDURE IF EXISTS EditOrders;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 27/05/2011
	Description:	<Edit Orders>
=============================================*/

CREATE PROCEDURE EditOrders(
_OrderID int,
_StateID int,
_OrderDate datetime,
_OrderFirstName varchar(100),
_OrderLastName varchar(100),
_OrderAddress varchar(255),
_OrderCity varchar(100),
_OrderZip varchar(100),
_OrderPhone varchar(50),
_OrderAlt varchar(50),
_OrderEmail varchar(50),
_OrderVIN varchar(50),
_OrderMileage varchar(100),
_OrderYearA int,
_OrderMake varchar(50),
_OrderModel varchar(50),
_OrderYearB int,
_OrderDP varchar(50),
_OrderMonthly int,
_OrderTerms varchar(100),
_OrderPolicyType varchar(255),
_OrderSaleDate datetime,
_OrderPostDate datetime,
_OrderFronter varchar(50),
_OrderCloser varchar(50),
_OrderPaymentInfo varchar(4),
_OrderNameOnCard varchar(100),
_OrderCC varchar(50),
_OrderExpDate datetime,
_OrderCVV varchar(50),
_OrderRouting varchar(50),
_OrderAcct varchar(50),
_OrderCheck varchar(50),
_OrderTemporaryPolicy varchar(255),
_OrderDateCreated datetime,
_OrderCreatorID int
)
BEGIN
	#SET NOCOUNT ON
	
		UPDATE ie_nap.Orders
		SET StateID= _StateID,
			OrderDate= _OrderDate,
			OrderFirstName= _OrderFirstName,
			OrderLastName= _OrderLastName,
			OrderAddress= _OrderAddress,
			OrderCity= _OrderCity,
			OrderZip= _OrderZip,
			OrderPhone= _OrderPhone,
			OrderAlt= _OrderAlt,
			OrderEmail= _OrderEmail,
			OrderVIN= _OrderVIN,
			OrderMileage= _OrderMileage,
			OrderYearA= _OrderYearA,
			OrderMake= _OrderMake,
			OrderModel= _OrderModel,
			OrderYearB= _OrderYearB,
			OrderDP= _OrderDP,
			OrderMonthly= _OrderMonthly,
			OrderTerms= _OrderTerms,
			OrderPolicyType= _OrderPolicyType,
			OrderSaleDate= _OrderSaleDate,
			OrderPostDate= _OrderPostDate,
			OrderFronter= _OrderFronter,
			OrderCloser= _OrderCloser,
			OrderPaymentInfo= _OrderPaymentInfo,
			OrderNameOnCard= _OrderNameOnCard,
			OrderCC= _OrderCC,
			OrderExpDate= _OrderExpDate,
			OrderCVV= _OrderCVV,
			OrderRouting= _OrderRouting,
			OrderAcct= _OrderAcct,
			OrderCheck= _OrderCheck,
			OrderTemporaryPolicy= _OrderTemporaryPolicy,
			OrderCreatorID= _OrderCreatorID
		WHERE OrderID=_OrderID;
		
END||

DELIMITER ;

/*====================================================================================================
5. CREATE SP DeleteOrders
=====================================================================================================*/

DROP PROCEDURE IF EXISTS DeleteOrders;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 27/05/2011
	Description:	<Delete Orders>
=============================================*/

CREATE PROCEDURE DeleteOrders(
_OrderID int
)
BEGIN
	#SET NOCOUNT ON
	
		UPDATE ie_nap.Orders
		SET OrderIsDelete= 1
		WHERE OrderID=_OrderID;
		
END||

DELIMITER ;

/*====================================================================================================
6. CREATE SP OrdersList
=====================================================================================================*/

DROP PROCEDURE IF EXISTS OrdersList;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 23/11/2015
	Description:	<Get Orders List>
=============================================*/

CREATE PROCEDURE OrdersList(
)
BEGIN
		
	SELECT OrderID,
		StateID,
		OrderDate,
		OrderFirstName,
		OrderLastName,
		OrderAddress,
		OrderCity,
		OrderZip,
		OrderPhone,
		OrderAlt,
		OrderEmail,
		OrderVIN,
		OrderMileage,
		OrderYearA,
		OrderMake,
		OrderModel,
		OrderYearB,
		OrderDP,
		OrderMonthly,
		OrderTerms,
		OrderPolicyType,
		OrderSaleDate,
		OrderPostDate,
		OrderFronter,
		OrderCloser,
		OrderPaymentInfo,
		OrderNameOnCard,
		OrderCC,
		OrderExpDate,
		OrderCVV,
		OrderRouting,
		OrderAcct,
		OrderCheck,
		OrderTemporaryPolicy,
		OrderDateCreated,
		OrderCreatorID,
		OrderIsDelete
FROM ie_nap.orders
ORDER BY OrderID ASC;
			 
		
END||OrdersList

DELIMITER ;


/*====================================================================================================
7. CREATE SP OrdersByID
=====================================================================================================*/

DROP PROCEDURE IF EXISTS OrdersByID;

DELIMITER ||

/*=============================================
	CREATED by Saumir Navarro, on 23/11/2015
	Description:	<Get Orders By ID>
=============================================*/

CREATE PROCEDURE OrdersByID(
_OrderID int
)
BEGIN
		
	SELECT OrderID,
		StateID,
		OrderDate,
		OrderFirstName,
		OrderLastName,
		OrderAddress,
		OrderCity,
		OrderZip,
		OrderPhone,
		OrderAlt,
		OrderEmail,
		OrderVIN,
		OrderMileage,
		OrderYearA,
		OrderMake,
		OrderModel,
		OrderYearB,
		OrderDP,
		OrderMonthly,
		OrderTerms,
		OrderPolicyType,
		OrderSaleDate,
		OrderPostDate,
		OrderFronter,
		OrderCloser,
		OrderPaymentInfo,
		OrderNameOnCard,
		OrderCC,
		OrderExpDate,
		OrderCVV,
		OrderRouting,
		OrderAcct,
		OrderCheck,
		OrderTemporaryPolicy,
		OrderDateCreated,
		OrderCreatorID,
		OrderIsDelete
FROM ie_nap.orders
WHERE OrderID=_OrderID;

			 
		
END||OrdersList

DELIMITER ;


/*====================================================================================================
4. END TRANSACCION
=====================================================================================================*/
-- ROLLBACK /*en caso de que la muestra de datos despues del cambio no coincida con los resultados. */
-- COMMIT   /*cuando la muestra de datos despues del cambio coincide con los resultados. */