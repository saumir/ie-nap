/*====================================================================================================

CREATE TABLE (ORDER)

=====================================================================================================*/
-- SERVER:        ---
-- Database:      ---
-- TABLE:         Orders
-- =======================================
-- CREATED by Saumir Navarro, on 23/11/2015
-- MODIFIED by ---

 

/*====================================================================================================
1. Select Database
=====================================================================================================*/

USE  ie_nap;

/*====================================================================================================
2. BEGIN TRANSACTION
=====================================================================================================*/

START TRANSACTION;

/*====================================================================================================
3. CREATE TABLE Orders
=====================================================================================================*/

DROP TABLE IF EXISTS Orders;


CREATE TABLE Orders(
	OrderID int AUTO_INCREMENT NOT NULL,
	StateID int NOT NULL,
	OrderDate datetime NOT NULL,
	OrderFirstName varchar(100) NOT NULL,
	OrderLastName varchar(100) NOT NULL,
	OrderAddress varchar(255) NOT NULL,
	OrderCity varchar(100) NOT NULL,
	OrderZip varchar(100) NOT NULL,
	OrderPhone varchar(50) NOT NULL,
	OrderAlt varchar(50) NOT NULL,
	OrderEmail varchar(50) NOT NULL,
	OrderVIN varchar(50) NOT NULL,
	OrderMileage varchar(100) NOT NULL,
	OrderYearA int NOT NULL,
	OrderMake varchar(50) NOT NULL,
	OrderModel varchar(50) NOT NULL,
	OrderYearB int NOT NULL,
	OrderDP varchar(50) NOT NULL,
	OrderMonthly int NOT NULL,
	OrderTerms varchar(100) NOT NULL,
	OrderPolicyType varchar(255) NOT NULL,
	OrderSaleDate datetime NOT NULL,
	OrderPostDate datetime NOT NULL,
	OrderFronter varchar(50) NOT NULL,
	OrderCloser varchar(50) NOT NULL,
	OrderPaymentInfo varchar(4) NOT NULL,
	OrderNameOnCard varchar(100) NOT NULL,
	OrderCC varchar(50) NOT NULL,
	OrderExpDate datetime NOT NULL,
	OrderCVV varchar(50) NOT NULL,
	OrderRouting varchar(50) NOT NULL,
	OrderAcct varchar(50) NOT NULL,
	OrderCheck varchar(50) NOT NULL,
	OrderTemporaryPolicy varchar(255) NOT NULL,
	    
	OrderDateCreated datetime NOT NULL,
	OrderCreatorID int NOT NULL,
    OrderIsDelete bit  NOT NULL DEFAULT 0, /*True=Yes; False=No*/
 PRIMARY KEY (OrderID));
 

/*====================================================================================================
4. CREATE TABLE States
=====================================================================================================*/

DROP TABLE IF EXISTS States;


CREATE TABLE States(
	StateID int AUTO_INCREMENT NOT NULL,
	StateName varchar (50) NULL,
    StateDateCreated datetime NOT NULL,
	StateCreatorID int NOT NULL,
    StateIsActive bit  NOT NULL DEFAULT 1, /*True=Yes; False=No*/
    StateIsDelete bit  NOT NULL DEFAULT 0, /*True=Yes; False=No*/
 PRIMARY KEY (StateID));



/*====================================================================================================
5. END TRANSACTION
=====================================================================================================*/
-- ROLLBACK /* if the data sample after the change does not match the results. */
-- COMMIT   /* when the sample data after the change coincides with the results. */